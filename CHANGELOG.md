# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- !4794 Diverses corrections sur les RSS des forums
